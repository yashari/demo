<?php

namespace App\Http\Middleware;

use Closure;

class JWTAuthenticate
{
    public function handle($request, Closure $next)
    {
        if (!auth()->check()) {
            return response()->json([
                'error' => 'Unauthorized'
            ], 401);
        }

        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;

class JWTRedirectIfAuthenticated
{
    public function handle($request, Closure $next)
    {
        if (auth()->check()) {
            return redirect(RouteServiceProvider::HOME);
        }

        return $next($request);
    }
}

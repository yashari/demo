FROM yashar/laravel:7.4

COPY --chown=www:www . /var/www

RUN composer install \
    --no-dev \
    --no-ansi \
    --no-interaction \
    --no-progress \
    --no-scripts \
    --prefer-dist \
    --optimize-autoloader \
    --classmap-authoritative

RUN composer dump
